/*
 * Copyright (C) 2019  Johannes Renkl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "../components"


Item {
    property var biller: null

    Component.onCompleted: {
        companyInputField.insert(biller.company);
        titleInputField.insert(biller.title);
        firstnameInputField.insert(biller.firstname);
        lastnameInputField.insert(biller.lastname);
        streetInputField.insert(biller.street);
        houseNumberInputField.insert(biller.houseNumber);
        postalCodeInputField.insert(biller.postalCode);
        cityInputField.insert(biller.city);
        phoneNumberInputField.insert(biller.phoneNumber);
        faxNumberInputField.insert(biller.faxNumber);
        ibanInputField.insert(biller.iban);
        bicInputField.insert(biller.bic);
        bankInputField.insert(biller.bank);
        taxIdInputField.insert(biller.taxId);
        currencyInputField.insert(biller.currency);
        biller.company = Qt.binding(function() {return companyInputField.text})
        biller.title = Qt.binding(function() {return titleInputField.text})
        biller.firstname = Qt.binding(function() {return firstnameInputField.text})
        biller.lastname = Qt.binding(function() {return lastnameInputField.text})
        biller.street = Qt.binding(function() {return streetInputField.text})
        biller.houseNumber = Qt.binding(function() {return houseNumberInputField.text})
        biller.postalCode = Qt.binding(function() {return postalCodeInputField.text})
        biller.city = Qt.binding(function() {return cityInputField.text})
        biller.phoneNumber = Qt.binding(function() {return phoneNumberInputField.text})
        biller.faxNumber = Qt.binding(function() {return faxNumberInputField.text})
        biller.iban = Qt.binding(function() {return ibanInputField.text})
        biller.bic = Qt.binding(function() {return bicInputField.text})
        biller.bank = Qt.binding(function() {return bankInputField.text})
        biller.taxId = Qt.binding(function() {return taxIdInputField.text})
        biller.currency = Qt.binding(function() {return currencyInputField.text})
    }

    ScrollView {
        id: billerFormScrollView

        anchors {
            fill: parent
            bottomMargin: Qt.inputMethod.visible ? Qt.inputMethod.keyboardRectangle.height : 0
        }

        ColumnLayout {
            anchors.fill: parent
            Layout.alignment: Qt.AlignVCenter
            spacing: units.gu(3)
            RowLayout {
                spacing: units.gu(2)
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft

                FormTextInputField {
                    id: companyInputField
                    fieldName: i18n.tr('Company')
                    // TODO: get rid of explicit height here
                    Layout.minimumHeight: units.gu(6)
                    Layout.preferredWidth: (settingsPage.width-units.gu(6))/2
                }

                FormTextInputField {
                    id: titleInputField
                    fieldName: i18n.tr('Title')
                    Layout.minimumHeight: units.gu(6)
                    Layout.preferredWidth: (settingsPage.width-units.gu(6))/2
                }
            }

            RowLayout {
                spacing: units.gu(2)
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft

                FormTextInputField {
                    id: firstnameInputField
                    fieldName: i18n.tr('Firstname')
                    // TODO: get rid of explicit height here
                    Layout.minimumHeight: units.gu(6)
                    Layout.preferredWidth: (settingsPage.width-units.gu(6))/2
                }

                FormTextInputField {
                    id: lastnameInputField
                    fieldName: i18n.tr('Lastname')
                    Layout.minimumHeight: units.gu(6)
                    Layout.fillWidth: true
                }
            }

            RowLayout {
                spacing: units.gu(2)
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft

                FormTextInputField {
                    id: streetInputField
                    fieldName: i18n.tr('Street')
                    Layout.minimumHeight: units.gu(6)
                    Layout.fillWidth: true
                }

                FormTextInputField {
                    id: houseNumberInputField
                    fieldName: i18n.tr('Number')
                    Layout.minimumHeight: units.gu(6)
                    Layout.minimumWidth: units.gu(10)
                }
            }

            RowLayout {
                spacing: units.gu(2)
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft

                FormTextInputField {
                    id: postalCodeInputField
                    fieldName: i18n.tr('Postal code')
                    Layout.minimumHeight: units.gu(6)
                    Layout.minimumWidth: units.gu(15)
                }
                
                FormTextInputField {
                    id: cityInputField
                    fieldName: i18n.tr('City')
                    Layout.minimumHeight: units.gu(6)
                    Layout.fillWidth: true
                }
            }

            RowLayout {
                spacing: units.gu(2)
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft

                FormTextInputField {
                    id: phoneNumberInputField
                    fieldName: i18n.tr('Telephone number')
                    Layout.minimumHeight: units.gu(6)
                    Layout.preferredWidth: (settingsPage.width-units.gu(6))/2
                }

                FormTextInputField {
                    id: faxNumberInputField
                    fieldName: i18n.tr('Fax number')
                    Layout.minimumHeight: units.gu(6)
                    Layout.fillWidth: true
                }
            }
     
            FormTextInputField {
                id: ibanInputField
                fieldName: i18n.tr('IBAN')
                Layout.minimumHeight: units.gu(6)
                Layout.fillWidth: true
            }

            RowLayout {
                spacing: units.gu(2)
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft

                FormTextInputField {
                    id: bicInputField
                    fieldName: i18n.tr('BIC')
                    Layout.minimumHeight: units.gu(6)
                    Layout.preferredWidth: (settingsPage.width-units.gu(6))/2
                }

                FormTextInputField {
                    id: bankInputField
                    fieldName: i18n.tr('BANK')
                    Layout.minimumHeight: units.gu(6)
                    Layout.fillWidth: true
                }
            }
      
            FormTextInputField {
                id: taxIdInputField
                fieldName: i18n.tr('Tax Id')
                Layout.minimumHeight: units.gu(6)
                Layout.fillWidth: true
            }

            FormTextInputField {
                id: currencyInputField
                fieldName: i18n.tr('Currency')
                Layout.minimumHeight: units.gu(6)
                Layout.fillWidth: true
            }

            Rectangle {
                Layout.fillHeight: true
            }
        }
    }
}
