/*
 * Copyright (C) 2019  Johannes Renkl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UITK_Popups
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Ubility 1.0
import "../components"

UITK.Page {
    id: menuPage

    signal addClient()
    signal goSettings()

    signal showBillItemsByClient(int clientIndex)
    signal editClient(int clientIndex)
    signal removeClient(int clientIndex, int clientId)

    signal cancelBill(int billIndex, int billId)
    signal undoBill(int billIndex, int billId)
    signal showBillItemsByBill(int billIndex)
    //signal printBill(int billIndex)

    property var clients: null
    property var bills: null

    header: UITK.PageHeader {
        id: header
        title: i18n.tr('Ubility')

        trailingActionBar.actions: [
            UITK.Action {
                id: goSettings
                objectName: "goSettings"
                text: i18n.tr("Settings")
                iconName: "settings"
                onTriggered: {
                    menuPage.goSettings()
                }
            },
            UITK.Action {
                id: addClient
                objectName: "addClient"
                text: i18n.tr("Add Client")
                iconName: "contact-new"
                onTriggered: {
                    menuPage.addClient()
                }
            }
        ]

        extension: UITK.Sections {
            id: menuSections
            anchors.verticalCenter: parent.verticalCenter 
            anchors.horizontalCenter: parent.horizontalCenter 

            actions: [
                UITK.Action {
                    text: i18n.tr("Clients")
                    onTriggered: {
                        billsScrollView.visible = false;
                        clientsColumn.visible = true;
                    }
                },
                UITK.Action {
                    text: i18n.tr("Bills")
                    onTriggered: {
                        clientsColumn.visible = false;
                        billsScrollView.visible = true;
                    }
                }
            ]
        }
    }

    ColumnLayout {
        id: clientsColumn
        anchors.topMargin: header.height + units.gu(2)
        anchors.fill: menuPage

        spacing: units.gu(2)

        UITK.TextField {
            id: searchInputField
            Layout.fillWidth: true
            Layout.leftMargin: units.gu(2)
            Layout.rightMargin: units.gu(2)
            placeholderText: i18n.tr("Search Client by name...")
            z: 500

            Component.onCompleted: {
                clients.searchFilter = Qt.binding(function() {return text})
            }
        }

        ScrollView {
            id: clientsScrollView
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop

            ListView {
                id: clientListView
                anchors.fill: parent
                header: Divider {}
                footer: Divider {}

                model: clients

                delegate: ClientListItem {
                    firstname: model.firstname
                    lastname: model.lastname

                    onRemove: {
                        UITK_Popups.PopupUtils.open(removeClientConfirmationDialog)
                    }

                    onEdit: {
                        menuPage.editClient(model.index);
                    }

                    onClicked: {
                        menuPage.showBillItemsByClient(model.index);
                    }

                    Component {
                        id: removeClientConfirmationDialog
                        ConfirmationPopup {
                            onConfirmed: menuPage.removeClient(model.index, model.cid)
                            text: i18n.tr("Do you really want to remove %1 %2?")
                                                .arg(model.firstname).arg(model.lastname)
                            confirmButtonText: i18n.tr("Delete")
                            confirmButtonColor: UITK.UbuntuColors.red
                        }
                    }
                }
            }
        }
    }

    ScrollView {
        id: billsScrollView
        anchors.top: parent.top
        anchors.topMargin: header.height
        anchors.fill: parent

        visible: false

        ListView {
            id: billListView
            anchors.fill: parent
            footer: Divider {}

            model: bills

            delegate: BillListItem {
                billId: model.bid
                clientName: model.clientname
                total: model.billingamount
                dueDate: model.dateRespite

                onCancel: {
                    UITK_Popups.PopupUtils.open(cancelBillConfirmationDialog)
                }

                onUndo: {
                    UITK_Popups.PopupUtils.open(undoBillConfirmationDialog)
                }

                onClicked: {
                    menuPage.showBillItemsByBill(model.index);
                }

                //onPrint: {
                //    menuPage.printBill(model.index);
                //}

                Component {
                    id: cancelBillConfirmationDialog
                    ConfirmationPopup {
                        onConfirmed: menuPage.cancelBill(model.index, model.bid)
                        text: i18n.tr("Do you really want to cancel bill %1? The bill and all its items will be removed. This action is not undoable!")
                                            .arg(model.bid)
                        confirmButtonText: i18n.tr("Delete bill and its items")
                        confirmButtonColor: UITK.UbuntuColors.red
                    }
                }

                Component {
                    id: undoBillConfirmationDialog
                    ConfirmationPopup {
                        onConfirmed: menuPage.undoBill(model.index, model.bid)
                        text: i18n.tr("Do you really want remove the bill %1 and reset all its items to status 'Not yet accounted'?").arg(model.bid)
                        confirmButtonText: i18n.tr("Yes, 'undo' bill")
                        confirmButtonColor: UITK.UbuntuColors.red
                    }
                }
            }
        }
    }
}
