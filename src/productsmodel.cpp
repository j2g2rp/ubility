#include "productsmodel.h"

#include <QDebug>
#include <QSqlRecord>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlField>
#include <QDate>

ProductsModel::ProductsModel(QSqlDatabase database, QObject *parent)
    : QSqlTableModel(parent, database)
{
    QSqlQuery query(database);
    if (!query.exec("create table if not exists products "
                    "(prodid integer primary key, "
                    "title varchar(30), "
                    "description varchar(100), "
                    "price float, "
                    "tax float)")) {
        qWarning() << "products table creation: " << lastError().text();
        return;
    }

    setTable("products");
    if (!select()) {
        qWarning() << "Failed to select table products: " << lastError().text();
    }
}

QVariant ProductsModel::data(const QModelIndex &index, int role) const
{
    QVariant value;

    if (index.isValid()) {
        if (role < Qt::UserRole) {
            value = QSqlTableModel::data(index, role);
        } else {
            int columnIdx = role - Qt::UserRole - 1;
            QModelIndex modelIndex = this->index(index.row(), columnIdx);
            value = QSqlTableModel::data(modelIndex, Qt::DisplayRole);
        }
    }
    return value;
}

QHash<int, QByteArray> ProductsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    for (int i = 0; i < this->record().count(); i++) {
        roles.insert(Qt::UserRole + i + 1, record().fieldName(i).toUtf8());
    }
    return roles;
}

QSqlRecord ProductsModel::createRecord(const QString &title, const QString &description, double price, double tax) const
{
    QSqlRecord newRecord;
    newRecord.append(QSqlField("title", QVariant::String));
    newRecord.append(QSqlField("description", QVariant::String));
    newRecord.append(QSqlField("price", QVariant::Double));
    newRecord.append(QSqlField("tax", QVariant::Double));
    newRecord.setValue("title", title);
    newRecord.setValue("description", description);
    newRecord.setValue("price", price);
    newRecord.setValue("tax", tax);
    return newRecord;
}

bool ProductsModel::add(const QString &title, const QString &description, double price, double tax)
{
    QSqlRecord newRecord = createRecord(title, description, price, tax);
    if (!insertRecord(-1, newRecord)) {
        qWarning() << "Failed to insert record into product database: " << lastError().text();
        return false;
    }
    select();
    return true;
}

bool ProductsModel::remove(int index)
{
    beginRemoveRows(QModelIndex(), index, index);
    removeRow(index);
    endRemoveRows();
    return true;
}

Product ProductsModel::get(int index) const
{
    QSqlRecord rec = record(index);
    Product product(rec.field("title").value().toString(),
                    rec.field("description").value().toString(),
                    rec.field("price").value().toDouble(),
                    rec.field("tax").value().toDouble(),
                    rec.field("id").value().toInt());
    return product;
}

ProductsModel::~ProductsModel() {
    database().close();
}
